<?php
Class ConsoleBuilding{

    private $arr_name_keywords  = ['1','__halt_compiler', 'abstract', 'and', 'array', 'as', 'break', 'callable', 'case', 'catch', 'class', 'clone', 'const', 'continue', 'declare', 'default', 'die', 'do', 'echo', 'else', 'elseif', 'empty', 'enddeclare', 'endfor', 'endforeach', 'endif', 'endswitch', 'endwhile', 'eval', 'exit', 'extends', 'final', 'for', 'foreach', 'function', 'global', 'goto', 'if', 'implements', 'include', 'include_once', 'instanceof', 'insteadof', 'interface', 'isset', 'list', 'namespace', 'new', 'or', 'print', 'private', 'protected', 'public', 'require', 'require_once', 'return', 'static', 'switch', 'throw', 'trait', 'try', 'unset', 'use', 'var', 'while', 'xor'];
    
    private $arr_option_create  = [
        'app',
        'controller',
        'model',
        'views',
        'view_list',
        'view_form',
        'migration'
    ];  

    public  $options            = null;

    public function __construct( Getopt $getopt = null )
    {
        $this->options = $getopt->getOptions(); 

        $create        = isset($this->options['create']) ? $this->options['create'] : false;
        $name          = isset($this->options['name'])   ? $this->options['name']   : false;
        $showhelp      = isset($this->options['help'])   ? $this->options['help']   : false;
        $execute       = isset($this->options['execute']) ? $this->options['execute']   : false;    
        $version       = isset($this->options['version']) ? $this->options['version']   : 0;    
        $listmigrations= isset($this->options['listmigrations']) ? $this->options['listmigrations']   : false;
        
        $msg_error     = false;

        if(!count($this->options) || $showhelp )
        {   
            echo $getopt->getHelpText( 35 );
            return false;
        }
            
        if(in_array($create, $this->arr_option_create ))
        {     
            if(!$name)
            {           
                $msg_error = 'Need include option --name or -n';
            }               
            else if(in_array($name, $this->arr_name_keywords))
            {   
                $msg_error = ' Name is word reserved ';
            }

        }
        else if(trim($create) != '' )
        {   
            $msg_error = ' Error create argument ['.$create.']';
        }
                
        if($msg_error)
        {       
            ConsoleHelper::error($msg_error);
        }   
        
        switch ($create) {
            case 'app':  
                $this->new_model( $name );
                $this->new_controller( $name );
                $this->new_views( $name );
                break;
            case 'model':
                $this->new_model( $name );
                break;  
            case 'controller':  
                $this->new_controller( $name );
                break;  
            case 'views':
                $this->new_views( $name );
                break;
            case 'view_list':
                $this->new_view_list( $name );
                break;
            case 'view_form':
                $this->new_view_form( $name );
                break;  
            case 'migration':
                $this->new_migration( $name );
                break;
        }

        if($execute)
        {   
            $this->execute_migration( $version);
        }
        if($listmigrations)
        {
            $this->list_migrations();
        }       
    }


    private function list_migrations()
    {
        $response       = $this->_call_migration("get_migrations");   
        $last_migration = $this->last_migration();
            
        if($response->status)
        {           
            ConsoleHelper::message(" Migration \t\tClass",'light_gray');

            foreach ($response->migrations as $migration => $value) {   
                $class = ucfirst( str_replace($migration."_","",  basename($value)) );
                if($migration<$last_migration)
                {   
                    ConsoleHelper::message(" $migration \t\t\t".$class,'light_green');
                }
                else
                {
                    ConsoleHelper::message(" $migration \t\t\t".$class,'yellow');
                }
                
            }
        }   
        else
        {
            ConsoleHelper::error(" Error on list migrations \n ".$response->msg);
        }
    }

    private function last_migration()
    {
        if( $content =  file("dependencies/migrations.log") )
        {          
            $last_migration = end($content);
            $parms = explode(",",$last_migration);
            return @$parms[1];  
        }
        return 0;
    }
    
    private function execute_migration( $version = 0 )
    {

        $last_migration  = $this->last_migration(); 
        
        $response        = $this->_call_migration("execute", $version, $last_migration ); 
        
        if($response->status)
        {   
            if(isset( $response->last_version) )
            {                   
                if( $fp = fopen("dependencies/migrations.log","a+") )
                {   
                    //save date & version
                    $line = date("YmdHis").",".$response->last_version."\n";
                    
                    fwrite($fp,  $line );   
                    fclose($fp); 
                }   
                else
                {
                    ConsoleHelper::error(" can't save log migrations " );
                }
                ConsoleHelper::success(' Migration '. $response->last_version.' was execute ');
            }
            else
            {   
                ConsoleHelper::success($response->msg);
            }
        }   
        else
        {           
            ConsoleHelper::error(" Error on execute migration \n ".$response->msg);
        }
    }
    
    private function new_migration( $myname )
    {

        $response       = $this->_call_migration("get_migrations");
        
        $myname         = strtolower(trim($myname) );
        $dir            = APPPATH.'migrations/';
        
        if($response->status)
        {   
                
            if($response->type =='sequential'){ 
                            
                $last = count( (array) $response->migrations);
                $next = $last + 1;    
                $name = sprintf('%03d', $next) . '_'. $myname;   

            } else if($response->type =='timestamp'){
                $name = date('YmdHis')."_".trim($myname);
            }

            $file_migration     = APPPATH.'migrations/'.$name.'.php';
            $new_migration_name = "$myname.php";

            
            foreach ($response->migrations as $migration => $value) {   
                $current_migration = strtolower( str_replace($migration."_","",  basename($value)) );
                if($new_migration_name==$current_migration)
                {
                    ConsoleHelper::error(' Migration name already exists');
                }
            } 
            
        } else{ 
              ConsoleHelper::error($response->msg);
        }

        if(!file_exists($dir))
        {
            if( mkdir( $dir , 0777, true) )
            {
                ConsoleHelper::success(' Folder migration was created ');
            }   
            else
            {   
                ConsoleHelper::error(" Can't be created folder in ".$dir );
            }
        }

        $names['{{MIGRATION_NAME}}']    = "Migration_".$myname;
        $names['{{MIGRATION_FILE}}']    = $name;
        $names['{{MIGRATION_COLUMNS}}'] = '';   
        $names['{{TABLE}}']             = isset( $this->options['table'] ) ? strtolower($this->options['table']) : 'unknow';             

        $columns = (isset( $this->options['cols'] ) && $this->options['cols']!=1 ) ? 
                $this->options['cols'] : 
                false;

        if( $columns )
        {   
            $arr_columns = explode(",",$columns);
            $columns_txt = "";

            foreach ($arr_columns as $key => $field ) {
                
                $data_column = explode(":",$field );

                $name = isset($data_column[0]) ? $data_column[0] : 'unknow';
                $type = isset($data_column[1]) ? $data_column[1] : 'unknow';
                    
                $data_scale = $this->_get_data_scale($type);
                
                $constrain = ($data_scale) ?  " 
                    'constrain' => '".$data_scale."', " : "";
                $columns_txt.= ", 
                '".$name."' => [
                    'type'   => '".$type."',
                    'null'   => FALSE,
                    ".$constrain."
                ] "; 
            }   

            $names['{{MIGRATION_COLUMNS}}'] = $columns_txt;
        }

        if( $this->_save_file(   
                        $file_migration, 
                        $this->_get_content_and_replace( 'migration_template.txt', $names ) 
                    ) )
        {           
            ConsoleHelper::success(' Migration '.$names['{{MIGRATION_FILE}}'].' was created');
        }        
    } 

    private function new_model($name )
    {
        $strtolower = strtolower( trim($name) );

        $names = [
            '{{MODEL_NAME_CLASS}}' => ucfirst($strtolower)."_model",
            '{{MODEL_TABLE}}' => isset( $this->options['table'] ) ? $this->options['table'] : $strtolower,
            '{{MODEL_FILE}}' => ucfirst($strtolower)."_model"
        ];
            
        $file_controller = APPPATH.'models/'.$names['{{MODEL_FILE}}'].'.php';   

        if( $this->_save_file(   
                $file_controller, 
                $this->_get_content_and_replace( 'model_template.txt', $names ) )
        )
        {
            ConsoleHelper::success(' Model '.$name.' was created');
        }   
    }   

    private function new_controller(  $name )
    {
        $strtolower = strtolower( trim($name) );
            
        $names = [      
            '{{CONTROLLER_NAME_CLASS}}' => ucfirst($strtolower),
            '{{CONTROLLER_NAME_URL}}' => $strtolower, 
            '{{CONTROLLER_DESCRIPTION}}' => str_replace("_"," ",ucfirst($strtolower)),
            '{{MODEL_NAME_CLASS}}' => ucfirst($strtolower)."_model",
            '{{VIEW_LIST}}' =>  $strtolower."/view-list-".$strtolower,
            '{{VIEW_FORM}}' =>  $strtolower."/view-form-".$strtolower
        ];  
            
        $file_controller = APPPATH.'controllers/'.$names['{{CONTROLLER_NAME_CLASS}}'].'.php';

        if( $this->_save_file(    $file_controller,  $this->_get_content_and_replace( 'controller_template.txt', $names ) ) )
        {           
            ConsoleHelper::success(' Controller '.$name.' was created');
        }           
    }       

    private function new_views($name)
    {

        $strtolower = strtolower($name);

        $names = [
            '{{CONTROLLER_URL}}'         => $strtolower,
            '{{CONTROLLER_DESCRIPTION}}' => str_replace("_"," ",$strtolower)  
        ];  

        $dir       = APPPATH.'views/'.$strtolower.'/';
        $file_list = $dir."/view-list-".$strtolower.'.php';
        $file_form = $dir."/view-form-".$strtolower.'.php';
       	
        
    	if( !file_exists($dir) )
        {
            if( mkdir( $dir , 0777, true) )
            {           
                ConsoleHelper::success(' Folder '.strtolower($name).' was created');
            }   
            else
            {       
                ConsoleHelper::error(" Folder could not be created in \n ".$dir );
            }
        }

        if(!$this->_save_file(   
                        $file_list, 
                        $this->_get_content_and_replace( 'view_list_template.txt', $names ) 
                    ) )
        {
            return false;
        }
        if(!$this->_save_file(
                        $file_form, 
                        $this->_get_content_and_replace( 'view_form_template.txt', $names ) 
                    ))
        {
            return false;
        }

        ConsoleHelper::success(' Views '.$strtolower.' was created ');
    }       

    private function new_view_list($name)
    {
        $strtolower = strtolower($name);
        $route      = explode("/",$strtolower); 
            
        if(count($route)>1)
        {           
            $dir             = APPPATH.'views/'.$route[0].'/';    
            $name_controller = $route[1];      
            $file            = $dir . 'view-list-'.$name_controller.'.php';
        }       
        else
        {   
            $dir             = FALSE;  
            $name_controller = $strtolower;
            $file            = APPPATH.'views/view-list-'.$name_controller.'.php';
        }           

        $names = [
            '{{CONTROLLER_URL}}'         => $name_controller,
            '{{CONTROLLER_DESCRIPTION}}' => str_replace("_"," ",$name_controller)  
        ];  


        if($dir)
        {   
            if(!file_exists($dir))
            {   
                if( mkdir( $dir , 0777, true) )
                {           
                    ConsoleHelper::success(" Folder ".$route[0]." was created");
                }   
                else
                {       
                    ConsoleHelper::error(" Folder could not be created in \n ".$dir );
                }
            }

        }   
            
        if ( $this->_save_file(   
                        $file, 
                        $this->_get_content_and_replace( 'view_list_template.txt', $names ) 
                    ) )
        {
            ConsoleHelper::success(' View '.$name.' was created');
        }
    } 

    private function new_view_form($name)
    {
        $strtolower = strtolower($name);
        $route      = explode("/",$strtolower); 
        
        if(count($route)>1)
        {           
            $dir             = APPPATH.'views/'.$route[0].'/';    
            $name_controller = $route[1];
            $file            = $dir . 'view-form-'.$name_controller.'.php';
        }       
        else
        {   
            $dir             = FALSE;  
            $name_controller = $strtolower;
            $file            = APPPATH.'views/view-form-'.$name_controller.'.php';
        }        
        
        $names = [
            '{{CONTROLLER_URL}}'         => $name_controller,
            '{{CONTROLLER_DESCRIPTION}}' => str_replace("_"," ",$name_controller)  
        ];   

        if($dir)
        {
            if(!file_exists($dir))
            {       
                if( mkdir( $dir , 0777, true) )
                {   
                    ConsoleHelper::success(" Folder ".$route[0]." was created");
                }       
                else
                {       
                    ConsoleHelper::error(" Folder could not be created in \n ".$dir );
                }
            }
        }        
            
        if( $this->_save_file(
                        $file,
                        $this->_get_content_and_replace( 'view_form_template.txt', $names ) 
                    ))
        {   
            ConsoleHelper::success(' View '.$name.' was created'); 
        }
    }   

    private function _save_file( $file, $str )
    {
        consoleHelper::exist( $file );

        if( $fp = @fopen( $file , "w") )
        {   
            fputs($fp, $str);
            fclose($fp);        
            chmod($file, 0765);
            return true;    
        }
        else
        {       
            ConsoleHelper::error("Could not create / modify the file, check the folder permissions ");
        }          
    }  

    private function _get_content_and_replace( $file, $replace_names, $folder = 'template' )
    {
        $f = file_get_contents($folder.'/'. $file );
        return strtr($f,$replace_names);
    }

    private function _get_data_scale( $type_name )
    {
        
        $open   = explode("(",$type_name);
        $tmp    = isset($open[1]) ? $open[1] : false;
                
        if($tmp)
        {   
            $length  = explode(")",$tmp);    
            return $length[0];
        }else
        {
            return 0;
        }
    }   

    private function _call_migration( $controller, $version = 0, $current_version=0 )
    {
        $path  = ROOTPATH;
        $shell = "php ".$path."/index.php migrate/".$controller." {$version} {$current_version} ";
        
        if($response = shell_exec($shell))
        {
            $decode = json_decode( $response );
            if(is_object($decode))
            {   
                //die(print_r($decode,1));
                return $decode;
            }      
            else
            {
                return (object)['status' => 0, 'msg' => "Set settings enable database."];
            }
            
        }       
        else
        {     
            return (object)['status' => 0, 'msg' => "Can't use shell_exec on php"];
        }
    }
}