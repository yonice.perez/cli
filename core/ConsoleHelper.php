<?php
/**
 * Helper console on building scaffolding
 *
 * @package		CodeIgniter
 * @author 		Jonathan Q
 * @since		Version 1.0.0
*/		

	class ConsoleHelper{	
		
		public function error($str, $stop = 1)
	    {
	        echo " **CLI-WARNING**\n";  
	        echo "\033[1;31m".$str."\033[0m";
	        echo "\n";
	        if($stop){
	            exit();
	        }
    	}   	

    	public function success( $str )
	    {
	        echo "\033[1;32m".$str."\033[0m";
	        echo "\n";
	    }
	    
	    public function message( $str, $color = 'yellow' )
	    {
			$color_text['black']        = '0;30';
			$color_text['dark_gray']    = '1;30';
			$color_text['blue']         = '0;34';
			$color_text['light_blue']   = '1;34';
			$color_text['green']        = '0;32';
			$color_text['light_green']  = '1;32';
			$color_text['cyan']         = '0;36';
			$color_text['light_cyan']   = '1;36';
			$color_text['red']          = '0;31';
			$color_text['light_red']    = '1;31';
			$color_text['purple']       = '0;35';
			$color_text['light_purple'] = '1;35';
			$color_text['brown']        = '0;33';
			$color_text['yellow']       = '1;33';
			$color_text['light_gray']   = '0;37';
			$color_text['white']        = '1;37';
	        echo "\033[".$color_text[$color]."m".$str."\033[0m";
	        echo "\n";
	    }

	    public function exist( $file )
	    {
	    	if(file_exists($file))
	    	{	
	    		$text = is_dir($file) ? "Folder" : "File";
	    		self::error(" ".$text." already exists: \n ".$file);
	    	}
	    }
	    
	    public function copyDependencies()
	    {	
	    	$my_dependencies = Array( [	
	    			'file' 		=> 'controller.migrate',
	    			'path_copy'	=> APPPATH . 'controllers/Migrate.php' 
	    		],[	
	    			'file' 		=> 'library.migration',
	    			'path_copy'	=> APPPATH . 'libraries/Migration.php'
	    		],[
	    			'file' 		=> 'core.myloader',
	    			'path_copy'	=> APPPATH . 'core/MY_Loader.php'
	    		],[
	    			'file'		=> 'htaccess',
	    			'path_copy'	=> ROOTPATH . '/.htaccess'
	    		],[
	    			'file'		=> 'view.header',
	    			'path_copy' => APPPATH . 'views/themes/header-admin.php'
	    		],[	
	    			'file'		=> 'view.footer',
	    			'path_copy' => APPPATH . 'views/themes/footer-admin.php'
	    		]
	    	);		

	    	foreach ($my_dependencies as $key => $dependence) {
	    		if(!file_exists($dependence['path_copy']))
	    		{		
	    			$dir = dirname($dependence['path_copy']);
	    			if(!file_exists($dir))
	    			{		
	    				if(mkdir( $dir , 0777) )
	    				{	
						  	ConsoleHelper::message(" Folder ". $dir." was created " ,'yellow');
			       		}
	    				else
	    				{
	    					ConsoleHelper::error(" Could not create dependence folder in \n ".$dir);
	    				}
	    			}

	    			if( $fp = fopen( $dependence['path_copy'] , "w") )
			        {			
			        	if( $content = @file_get_contents('dependencies/'. $dependence['file'].'.txt' ))
			        	{
			        		fputs($fp,$content );
				            fclose($fp);	
				            chmod($dependence['path_copy'], 0765);
				            ConsoleHelper::message(" dependence was created ". $dependence['path_copy'] ,'yellow');
			       		}	
			       		else
			       		{
			       			ConsoleHelper::error(" dependence not found in \n cli/dependencies/".$dependence['file'].".txt");
			       		}
					}	
			        else
			        {	   
			            ConsoleHelper::error(" Could not create dependences file, check the permissions of the folder to save \n ".$dependence['path_copy']);
			        }
	    		}
	    	}
	    	echo "\n";

	    }	
	}
?>