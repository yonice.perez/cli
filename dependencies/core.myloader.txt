<?php
/**
*   route: /application/core/MY_Loader.php
*/          
class MY_Loader extends CI_Loader 
{ 
    /**
     * Template loader
     *
     * @param $view_name string name of view content
     * @param $vars_content array params for send in content
     * @param $return boolean return template or print html
     * @param $theme theme selected in folder views/theme
     *
     * @return html | string
     */
    public function template( $view_name , $vars_content = array(), $return = FALSE, $theme = 'admin')
    {       
        $vars_header['content']['messages'] = $this->get_messages();
        
        if($return)
        {   
            $content = $this->view('themes/header-'.$theme, $vars_header, TRUE);
            $content.= $this->view($view_name, $vars_content, TRUE);
            $content.= $this->view('themes/footer-'.$theme, null, TRUE);
            return $content;
        }           
        else
        {               
            $this->view('themes/header-'.$theme, $vars_header, FALSE);
            $this->view($view_name, $vars_content, FALSE);
            $this->view('themes/footer-'.$theme, null, FALSE);
        }
    }

    /**
     * Set message error
     *
     * Create message error with session
     *
     * @param $msg string to save error if not content set error form validation
     * 
     * @return void
     */
    public function set_message_error( $msg = '' )
    {   
        $CI =& get_instance();  
        if($msg === '')
        {        
            
            $errors = $CI->form_validation->error_array();
            foreach ( $errors as $key => $value) 
            {
                $msg.= "<p>".$value."</p>";
            }
        }

        $CI->session->set_userdata( array ( 'msgError' => $msg) ); 
    }

    /**
     * Set message success
     *
     * Create message success with session
     *
     * @param $msg string to save success
     * 
     * @return void
     */
    public function set_message_success( $msg = '' )
    {       
        $CI =& get_instance();
        $CI->session->set_userdata ( [ 'msgSuccess' => $msg ] ); 
    }   

    /**
     * Get messages
     *  
     * Messages success or errors with session
     * 
     * @return Array
     */
    private function get_messages()
    {
        /*
        *   Exist msgError?
        */
        $CI        =& get_instance();
        $msg_error =  $msg_success = '';

        if(isset( $CI->session->userdata['msgError']))
        {
            if( $CI->session->userdata['msgError']!='' )
            {      
                $msg_error.= '<div  id="alert-msg-error" class="alert alert-danger" role="alert">';
                $msg_error.=  $CI->session->userdata['msgError'].'</div>';  

                $msg_error.= '<script>
                        document.addEventListener("DOMContentLoaded", function(event) { 
                            setTimeout(function(){  
                                $("#alert-msg-error").slideToggle(500);
                            },8000);                    
                        });        
                    </script>';

                 $CI->session->set_userdata ( array( 'msgError' => '' ) );
            }   
        }

        /*
        *   Exist msgSuccess?
        */
       
        if(isset($CI->session->userdata['msgSuccess']))
        {
            if($CI->session->userdata['msgSuccess']!='')
            {   
                $msg_success.= '<div  id="alert-msg-success" class="alert alert-success" role="alert">';
                $msg_success.= $CI->session->userdata['msgSuccess'].'</div>';
                $msg_success.= '<script>
                        document.addEventListener("DOMContentLoaded", function(event) { 
                            setTimeout(function(){  
                                $("#alert-msg-success").slideToggle(500);
                            },5000);                
                        });     
                    </script>';

                $CI->session->set_userdata ( array ( 'msgSuccess' => '' ) );
            }   
        }

        return Array( 
                'msg_error'     => $msg_error, 
                'msg_success'   => $msg_success
            );
    }

}