# codeigniter-cli Generate scaffolding with console#

## Introduction:
This is the preliminary codeigniter comand line interface.
This project streamlines the speed and standardization of software for the model, view and controller of codeigniter.

## **Advantage**
:heavy_check_mark: Create files dependencies [htaccess, controller/migrate, core/my_loader, library/migration, views/header, views/footer ].

:heavy_check_mark: Create controller from a standar template.

:heavy_check_mark: Create model from a standar template.

:heavy_check_mark: Create views from a standar template.

:heavy_check_mark: Create full app controller, model and view in a step.

:heavy_check_mark: Create file migration in a step.

:heavy_check_mark: Execute migration pendings.

:heavy_check_mark: Execute migration asigned (eg. php codeigniter-cli -e --version 002 ).

:heavy_check_mark: Easy show logs migration (eg. php codeigniter-cli -l).

:heavy_check_mark: Assign read-only files when migration is up (chmod 0444).

## Requirements

* codeigniter 3.*
* Download folder cli on project /mycodeigniter/ or edit ROOTPATH to route proyect/application
* Activate config/autoload.php library [form_validation, session] **_not required_**
* Activate config/migration.php migration_enabled = TRUE **_not required_**
* Set type migration config/migration.php migration_type sequential **_not required_**
    
## Structure
```
.
├── codeigniter-cli
├── core
│   ├── ConsoleBuilding.php
│   ├── ConsoleHelper.php
│   └── Getopt.php
├── dependencies
│   ├── controller.migrate.txt
│   ├── core.myloader.txt
│   ├── htaccess.txt
│   ├── library.migration.txt
│   ├── migrations.log
│   ├── view.footer.txt
│   └── view.header.txt
├── README.md
├── resources
│   ├── img_1.png
│   ├── img_2.png
│   └── img_3.png
└── template
    ├── controller_template.txt
    ├── migration_template.txt
    ├── model_template.txt
    ├── view_form_template.txt
    └── view_list_template.txt
    
4 directories, 20 files
```
#### Folder dependencies
CI files to be dependent for proper operation, if not want to add any copyDependencies edit the function that is in **core/ConsoleHelper.php**

| File                    | Description |
| --------                | --------    |
| controller.migrate.txt  | Driver to work with migration runs only from the command line |
| core.myloader.txt       | Call for templates and error messages or validation |
| htaccess.txt            | remove index.php from url, show all messages errors if blocked apache
| library.migration.txt   | Add the error messages that lacked
| migrations.log          | Save date and migration up 
| view.footer.txt         | view footer for template core myloader
| view.header.txt         | view header for template core myloader
      
#### Folder template
It contains files that are added to the project. You can edit these files to tailor its **programming standard**

| Action 		  | File 		 				|
| -------- 		  | -------- 					|
| new Controller  | controller_template.txt   	|
| new Migration   | migration_template.txt   	|
| new Model  	  | model_template.txt   		|
| new View_form   | view_form_template.txt   	|
| new View_list   | view_list_template.txt   		|
  

## How does it work

First assign permissions to the core folder to avoid problems reading with templates or dependencies and position yourself in the folder.

```sh
  chmod -R 0777 cli/
  cd cli/
```

generate dependencies if not exist and show help
```sh
  php codeigniter-cli
```
![alt text](resources/img_1.png "Step 1, generate all dependences")

Create scafolding app
```sh
  php codeigniter-cli --create app --name myfirstapp --table "tablename"
```
> or using short syntax , note --table or -t not required
```sh
  php codeigniter-cli -c app -n myfirstapp
```

Create new full catalag products
```sh
  php codeigniter-cli -c app -n products -t products 
  php codeigniter-cli -c migrate  -n create_table_products -t products --cols "description:varchar(100),price:decimal(10,2)"
  php codeigniter-cli -e
```

Create migration users and see my versions
```sh
  php codeigniter-cli -c migrate -n create_table_users -t users --cols "user:varchar,password:varchar"
  php codeigniter-cli --listversion
```

after --listversion or -l you can se version list executed green or pending yellow
![alt text](resources/img_3.png "Migration log")

 
> be water my friend.